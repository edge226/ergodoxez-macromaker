To run this script you need to load the library script like so:
`. ./newmacro.sh`

There are two main library functions the user would want to use:
`newmacro "String"`
and
`nmm "String 1" "String 2"`

nmm(New multiple macros) will always write to newmacro.txt of the current directory.

The only niche case in operation is explained below.

The ! character is problematic and you must unquote and delimit it. Every other character on the US keymap functions correctly. So when running newmacro or nmm with a ! point in the string it should be run like so:

`newmacro "Start of the string"\!" Here is some more output after the "\!" Sign."`

Here is an example output from the above:
```
$ newmacro "Start of the string"\!" Here is some more output after the "\!" Sign."

## This is the definition for Start of the string! Here is some more output after the ! Sign.

#define START OF THE STRING! HERE IS SOME MORE OUTPUT AFTER THE ! SIGN.

## This is the button function for Start of the string! Here is some more output after the ! Sign.
M(START OF THE STRING! HERE IS SOME MORE OUTPUT AFTER THE ! SIGN.)

## This will be the case definition for Start of the string! Here is some more output after the ! Sign.


case START OF THE STRING! HERE IS SOME MORE OUTPUT AFTER THE ! SIGN.:
        if (record->event.pressed) {
          return MACRO(D(LSFT),T(S),U(LSFT),T(T),T(A),T(R),T(T),T(SPC),T(O),T(F),T(SPC),T(T),T(H),T(E),T(SPC),T(S),T(T),T(R),T(I),T(N),T(G),D(LSFT),T(1),U(LSFT),T(SPC),D(LSFT),T(H),U(LSFT),T(E),T(R),T(E
),T(SPC),T(I),T(S),T(SPC),T(S),T(O),T(M),T(E),T(SPC),T(M),T(O),T(R),T(E),T(SPC),T(O),T(U),T(T),T(P),T(U),T(T),T(SPC),T(A),T(F),T(T),T(E),T(R),T(SPC),T(T),T(H),T(E),T(SPC),D(LSFT),T(1),U(LSFT),T(SPC),D(L
SFT),T(S),U(LSFT),T(I),T(G),T(N),T(DOT),END);
        }
```

The tabs are 2 spaces and the output corresponds with how Atom would paste the data. This string creates a bad definition, `M()` and case statment but those are simple to fix.

The main goal was to have a working `return MACRO()` section since that is the most painful part of writing the macros.