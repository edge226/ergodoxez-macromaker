#!/usr/bin/env bash

# if [[ $(type -t cleanup_newmacro) ]]
#   ## Cleans the variables and functions of newmacro.
# then
#   cleanup_newmacro
# fi

isupper()
# This accepts a character at a time.
{
  if [[ "$1" =~ [A-Z\?\+\{\}\_\:\"\<\>\~\!\@\#\$\%\^\&\*\(\)\|] ]] && ! [[ "$1" =~ [a-z0-9\/\\\'\;\[\]\=\-\.\,] ]]
  then
    return 0
  else
    return 1
  fi
}
newmacrovariables+=("") ## Input used variables in here.
newmacrofunctions+=("isupper")

islower()
{
  if [[ "$1" =~ [^A-Z\?\+\{\}\_\:\"\<\>\~\!\@\#\$\%\^\&\*\(\)\|-] ]]
  then
    return 0
  else
    return 1
  fi
}
newmacrovariables+=("") ## Input used variables in here.
newmacrofunctions+=("islower")

issymbol()
{
  if [[ "$1" =~ [^A-Za-z-] ]] ## && [[ "$1" =~ [a-z] ]]
  then
    return 0
  else
    return 1
  fi
}
newmacrovariables+=("") ## Input used variables in here.
newmacrofunctions+=("issymbol")

toupper()
{
  csmacrotext="$1"
  macrotextupper=${csmacrotext^^}
  macrotextupper=${macrotextupper% }
  export macrotextupper
}
newmacrovariables+=("csmacrotext" "macrotextupper") ## Input used variables in here.
newmacrofunctions+=("toupper")

setkeys()
{
  export declare newline=$'\n'
  export declare tab=$'\t'
  export declare bksp=$'\b'
  declare -Ag keys=(
  [\_]="MINS"
  [\-]="MINS"
  [\+]="EQL"
  [\=]="EQL"
  [\{]="LBRC"
  [\[]="LBRC"
  [\}]="RBRC"
  [\]]="RBRC"
  [\\]="BSLS"
  [\|]="BSLS"
  [\`]="GRV"
  [\~]="GRV"
  [\:]="SCLN"
  [\;]="SCLN"
  [\"]="QUOT"
  [\']="QUOT"
  [\,]="COMM"
  [\<]="COMM"
  [\?]="SLSH"
  [\/]="SLSH"
  [\>]="DOT"
  [\.]="DOT"
  [\!]="1"
  [1]="1"
  [\@]="2"
  [2]="2"
  [\#]="3"
  [3]="3"
  [\$]="4"
  [4]="4"
  [\%]="5"
  [5]="5"
  [\^]="6"
  [6]="6"
  [\&]="7"
  [7]="7"
  [\*]="8"
  [8]="8"
  [\(]="9"
  [9]="9"
  [\)]="0"
  [0]="0"
  )
  export keys
}
newmacrovariables+=("keys") ## Input used variables in here.
newmacrofunctions+=("setkeys")


writemacro()
{
  macrotextlength=${#macrotext[0]}
  
  printf '%s' "          return MACRO("
  
  # Add associative array function here.
  setkeys
  
  for ((i=0; i<=$macrotextlength; i++))
  do
    if [[ "${macrotext[@]:$i:1}" = " " ]]
    then
      printf '%s' "T(SPC),"
    elif [[ "${macrotext[@]:$i:1}" = "$newline" ]]
    then
      printf '%s' "W(255),W(255),T(ENT),"
    # tab and backspace do not work correctly for some reason.
    elif [[ "${macrotext[@]:$i:1}" = "$bksp" ]]
    then
      printf '%s' "T(BKSP),"
    elif [[ "${macrotext[@]:$i:1}" = "$tab" ]]
    then
      printf '%s' "T(TAB),"
    elif isupper "${macrotext[@]:$i:1}"
    then
      currentchar=$(printf '%s' "${macrotext[@]:$i:1}")
      if issymbol "${macrotext[@]:$i:1}" && [[ ${keys[$currentchar]} != "" ]]
      then  
        printf '%s%s%s%s%s' "D(LSFT)," "T(" "${keys[$currentchar]}" ")," "U(LSFT),"
      else
        printf '%s%s%s%s%s' "D(LSFT)," "T(" "${macrotextupper[@]:$i:1}" ")," "U(LSFT),"
      fi
    elif islower "${macrotext[@]:$i:1}"
    then
      if issymbol "${macrotext[@]:$i:1}"
      then
        currentchar=$(printf '%s' "${macrotext[@]:$i:1}")
        printf '%s%s%s' "T(" "${keys[$currentchar]}" "),"
      else
        printf '%s%s%s' "T(" "${macrotextupper[@]:$i:1}" "),"
      fi
    else
      continue
    fi
  done
  
  printf '%s\n' "END);"
  
}
newmacrovariables+=("macrotextlength") ## Input used variables in here.
newmacrofunctions+=("writemacro")

newmacro()
{
  macrotext=$(printf '%s' "$@")
  toupper "$macrotext"
  
  printf '\n%s %s\n\n' "## This is the definition for" "$macrotext"
  printf '%s %s\n\n' "#define" "$macrotextupper"
  printf '%s %s\n' "## This is the button function for" "$macrotext"
  printf '%s%s%s\n\n' "M(" "$macrotextupper" ")"
  printf '%s %s\n\n' "## This will be the case definition for" "$macrotext"
  printf '%s %s%s\n%s\n' "case" "$macrotextupper" ":" "        if (record->event.pressed) {"
  writemacro
  printf '%s\n' "        }"
}
newmacrovariables+=("") ## Input used variables in here.
newmacrofunctions+=("newmacro")

nmm()
{
  for macro in "$@"
  do
    newmacro "$macro" >> newmacros.txt
  done
}
newmacrovariables+=("") ## Input used variables in here.
newmacrofunctions+=("nmm")

cleanup_newmacro() ## Cleanup function.
{
  newmacrofunctions+=("cleanup_newmacro")
  for function in "${newmacrofunctions[@]}"
  do
    unset -f "$function"
  done

  newmacrovariables+=("function" "_" "var")
  for var in "${newmacrovariables[@]}"
  do
    unset -v "$var"
  done
}